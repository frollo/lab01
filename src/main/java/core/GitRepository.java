package core;

import java.io.*;

public class GitRepository{
  private String repoPath;

  public GitRepository(String path){
    this.repoPath = path;
  }

  public String getHeadRef() throws Exception{
    try(BufferedReader bf = new BufferedReader(new FileReader(this.repoPath + "/HEAD"))){
      String line = bf.readLine();
      return line.substring(5);
    }catch(Exception e){
      throw e;
    }
  }
}
